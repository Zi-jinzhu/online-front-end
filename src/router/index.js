import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },

  {
    path: '/',
    component: Layout,
    redirect: '/teacher',
    hidden: true
  },
  {
    path: '/teacher',
    component: Layout,
    redirect: '/teacher/list', // teacher无页面重定向到teacher/list
    name: 'Teacher', // 路由名称
    meta: { title: '讲师管理', icon: 'eye' },
    children: [{
      path: 'list',
      name: 'TeacherList',
      meta: { title: '讲师列表', icon: 'table' },
      component: () => import('@/views/teacher/list')
    },
    {
      path: 'save',
      name: 'TeacherSave',
      meta: { title: '讲师新增', icon: 'form' },
      component: () => import('@/views/teacher/form')
    },
    {
      path: 'edit/:id', // /teacher/edit :id表示路径占位符，访问edit时
      // 需要在后面添加一层路径才可以访问，添加的那层路径会当做id变量的值
      name: 'TeacherEdit',
      hidden: true,
      meta: { title: '讲师更新', icon: 'form' },
      component: () => import('@/views/teacher/form')
    }]
  },
  {
    path: '/subject',
    component: Layout,
    redirect: '/subject/tree', // subject无页面重定向到/subject/tree
    name: 'Subject', // 路由名称
    meta: { title: '课程分类管理', icon: 'eye' },
    children: [{
      path: 'tree',
      name: 'SubjectTree',
      meta: { title: '课程分类列表', icon: 'tree' },
      component: () => import('@/views/subject/tree')// 引入页面所在位置/views/subject/tree
    },
    {
      path: 'importt',
      name: 'SubjectImort',
      meta: { title: '课程分类导入', icon: 'form' },
      component: () => import('@/views/subject/import')
    }]
  },
  {
    path: '/course',
    component: Layout,
    redirect: '/course/list', // subject无页面重定向到/subject/tree
    name: 'Course', // 路由名称
    meta: { title: '课程管理', icon: 'eye' },
    children: [{
      path: 'list',
      name: 'CourseList',
      meta: { title: '课程列表', icon: 'tree' },
      component: () => import('@/views/course/list')// 引入页面所在位置/views/subject/tree
    },
    {
      path: 'publish',
      name: 'CoursePublish',
      meta: { title: '课程发布', icon: 'form' },
      component: () => import('@/views/course/publish')
    },
    {
      path: 'edit/:id',
      name: 'CourseEdit',
      hidden: true,
      meta: { title: '课程更新', icon: 'form' },
      component: () => import('@/views/course/publish')
    }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
