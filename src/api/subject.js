import request from '@/utils/request'
export default{
  // 查询课程分类嵌套集合,一级标签里带着二级标签
  nestedSubjects() {
    return request({
      url: '/admin/edu/subject/getNestedSubject',
      method: 'get'
    })
  }
}
