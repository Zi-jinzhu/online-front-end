import request from '@/utils/request'
export default{
  save(video) {
    return request({
      url: '/admin/edu/video/save',
      method: 'post',
      data: video
    })
  },
  getById(id) {
    return request({
      url: `/admin/edu/video/get/${id}`,
      method: 'get'
    })
  },
  update(video) {
    return request({
      url: '/admin/edu/video/update',
      method: 'put',
      data: video
    })
  },
  delete(id) {
    return request({
      url: `/admin/edu/video/delete/${id}`,
      method: 'delete'
    })
  }
}
