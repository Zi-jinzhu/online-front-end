import request from '@/utils/request'
export default{
  // 发布课程
  publish(courseId) {
    return request({
      url: `/admin/edu/course/publish/${courseId}`,
      method: 'put'
    })
  },
  // 查询要发布课时的详情
  getCoursePublishInfo(courseId) {
    return request({
      url: `/admin/edu/course/getCoursePublishInfo/${courseId}`,
      method: 'get'
    })
  },
  // 分页
  list(pageNum, pageSize) {
    return request({
      url: `/admin/edu/course/list/${pageNum}/${pageSize}`,
      method: 'get'
    })
  },
  getById(id) {
    return request({
      url: `/admin/edu/course/get-CourseInfo/${id}`,
      method: 'get'
    })
  },
  update(courseInfo, id) { // 小心参数顺序
    return request({
      url: `/admin/edu/course/update-CourseInfo/${id}`,
      method: 'put',
      data: courseInfo
    })
  },
  // 保存课程基本信息
  saveCourseInfo(courseInfo) {
    return request({
      url: '/admin/edu/course/save-CourseInfo',
      method: 'post',
      data: courseInfo
    })
  }
}
