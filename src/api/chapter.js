import request from '@/utils/request'
export default{
  // 查询章课时嵌套集合
  getChaptersAndVideos(courseId) {
    return request({
      url: `/admin/edu/video/getChaptersAndVideos/${courseId}`,
      method: 'get'
    })
  },
  save(chapter) {
    return request({
      url: '/admin/edu/chapter/save',
      method: 'post',
      data: chapter
    })
  },
  getById(id) {
    return request({
      url: `/admin/edu/chapter/get/${id}`,
      method: 'get'
    })
  },
  update(chapter) {
    return request({
      url: '/admin/edu/chapter/update',
      method: 'put',
      data: chapter
    })
  },
  delete(id) {
    return request({
      url: `/admin/edu/chapter/delete/${id}`,
      method: 'delete'
    })
  }
}
