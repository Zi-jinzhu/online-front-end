import request from '@/utils/request'
export default{

  // 根据id查询讲师
  getById(id) {
    return request({
      url: `/admin/edu/teacher/getById/${id}`,
      method: 'get'
    })
  },
  // 根据id更新讲师
  update(teacher) {
    return request({
      url: `/admin/edu/teacher/updateById`,
      method: 'put',
      data: teacher
    })
  },
  // 新增讲师
  save(teacher) {
    return request({
      url: '/admin/edu/teacher/save',
      method: 'post',
      data: teacher
    })
  },

  // 批量删除
  batchDel(ids) {
    return request({
      url: '/admin/edu/teacher/batchDel/',
      method: 'DELETE',
      data: ids // ids代表要删除的json数组 [1,2,3]
    })
  },
  // 带条件的分页
  page(pageNum, pageSize, searchObj) { // page是json对象里的属性
    return request({
      url: `/admin/edu/teacher/pageList/${pageNum}/${pageSize}`,
      method: 'GET',
      params: searchObj// 请求参数，axios会自动将params中的kv属性值拼接到请求地址后
    })
  },
  // 查询讲师列表
  list() {
    return request({
      url: '/admin/edu/teacher/list',
      method: 'GET'
    })
  },
  // 删除讲师
  deleteById(id) {
    return request({
      url: `/admin/edu/teacher/deleteById/${id}`,
      method: 'DELETE'
    })
  }
}
